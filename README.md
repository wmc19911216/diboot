> 用上Diboot，借助强大易用的代码生成器，告别常规SQL和CRUD，写的更少，性能更好！

> 新用户指南:
- [开发一个CRM模块，不用手写一行代码](https://www.bilibili.com/video/BV1xF411S7eF/)

- [开发一个OA系统，只用3天](https://www.bilibili.com/video/BV13N411r7qz/)

- [了解 Diboot内核如何做到高性能](https://www.bilibili.com/video/BV1tL411p7CD)

> 上手体验:
- [开始体验，从这里开始](https://www.diboot.com/guide/beginner/bootstrap.html)

# Diboot - 基础组件化繁为简，高效工具以简驭繁
<hr>
<p align="center">
    <a href="http://www.apache.org/licenses/LICENSE-2.0.html" target="_blank">
        <img src="https://img.shields.io/hexpm/l/plug.svg">
    </a>
    <a href="https://mvnrepository.com/artifact/com.diboot" target="_blank">
        <img src="https://img.shields.io/maven-central/v/com.diboot/diboot-core-spring-boot-starter">
    </a>
</p>

> 设计目标: 为开发人员打造的低代码开发平台，将复杂的工作简单化、重复的工作自动化，提高质量、效率、可维护性。

![diboot平台体系架构图](https://www.diboot.com/img/diboot_structure.png)

> [零代码、工作流、微服务，更强的企业版 看这里->](https://www.diboot.com/edition/enterprise.html)

## 1. Diboot 优势特性

### 基础框架 ～ 简单
- 用上Diboot，大多数SQL都不用写了，代码极大简化，让开发专注于业务；
- Diboot基础封装的内部实现，确保运行高效率高性能，帮你规避常见的坑。

### 低代码能力 ～ 卓越
- 基于Devtools的代码生成能力，后端、前端、关联、复杂主子页面、移动端、非覆盖式更新代码、AI辅助命名... 全支持；
- 零代码不能满足的复杂场景，基于生成后的代码快速扩展，无任何扩展局限性。

### 零代码能力 ～ 强大
- 模型设计、页面设计、表单设计、流程设计、数据大屏 等能力全支持，多数功能直接配；
- 表单与流程解耦，流程挂载多表单，表单灵活复用。

### 零/低/全代码之间顺畅融合，自由切换
- 多数常规功能零代码搭建即用；
- 复杂功能支持集成自定义手写页面、支持生成前后端代码自由扩展；

## 2. 配套前端框架部分页面预览（diboot-admin-ui）
> 基于 Vue3 + Vite + Pinia + Element-plus + TypeScript 的自研中后台管理框架

![菜单资源配置](https://www.diboot.com/img/permission.png)

![角色权限配置](https://www.diboot.com/img/role-permission.png)


## 3. 技术交流&支持
### 支持Diboot：
  * Diboot及Devtools完全公益化运营，如您想要支持我们，可通过捐助 或者 顺手点个star，感谢每一份信任与鼓励!
  <img src="https://www.diboot.com/wechat_donate.png" width="240">
  
### 使用过程中，如遇相关技术问题，欢迎加群交流：

  * 技术交流QQ群: [731690096]()

  * 技术交流微信群 加微信 [wx20201024] (备注diboot): 
  * <img src="https://www.diboot.com/add_wechat.png" width="240">