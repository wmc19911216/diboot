const zhCN = {
  login: {
    tenantCode: '租户编码',
    username: '用户名',
    password: '密码',
    captcha: '验证码',
    rules: {
      tenantCode: '请填写租户编码',
      username: '请填写用户名',
      password: '请填写密码',
      captcha: '请填写验证码'
    },
    submit: '登录'
  }
}

export type Locale = typeof zhCN

export default zhCN
