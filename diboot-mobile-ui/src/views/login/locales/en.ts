import type { Locale } from './zhCN'

const en: Locale = {
  login: {
    tenantCode: 'TenantCode',
    username: 'username',
    password: 'password',
    captcha: 'captcha',
    rules: {
      tenantCode: 'Please Input TenantCode',
      username: 'Please Input Username',
      password: 'Please Input Password',
      captcha: 'Please Input Captcha'
    },
    submit: 'Login'
  }
}

export default en
