package diboot.core.test.binder.service;

import com.diboot.core.service.BaseService;
import diboot.core.test.binder.entity.Pet;

/**
* 宠物 相关Service接口定义
* @author MyName
* @version 1.0
* @date 2024-09-03
* Copyright © MyCorp
*/
public interface PetService extends BaseService<Pet> {

}