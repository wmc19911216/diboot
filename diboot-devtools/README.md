# Diboot-devtools 开发工具 简介
## devtools的特性：
* 极简易用（引入依赖jar，配置参数后，即可随应用启动运行）
* 功能强大（数据结构与代码同步、前后端代码一键生成、非覆盖式更新后端代码、彻底摆脱CRUD）
* 代码标准（devtools标准化了数据结构定义与基础代码实现，更易维护）
* 支持多库（MySQL、MariaDB、PostgreSQL、Oracle、SqlServer、达梦DM、金仓Kingbase、SQLite等）

## 使用方式

### 1. Devtools 新用户：
如果您是Diboot新用户，强烈建议先玩转 [新手体验营](https://www.diboot.com/guide/beginner/bootstrap.html)，
这篇教程将带你用15分钟完成：项目创建、初始化、以及一个完整的功能开发，而且是**不写一行代码**。

### 2. Devtools老用户：
可以自行从playground项目中下载devtools jar包自行配置使用
[下载 devtools jar](https://gitee.com/dibo_software/playground/tree/master-v3/demo/libs)。