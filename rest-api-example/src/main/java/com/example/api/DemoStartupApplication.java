package com.example.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoStartupApplication {

    public static void main(String[] args) {
        SpringApplication.run(DemoStartupApplication.class, args);
    }

}
